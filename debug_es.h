#charset "utf-8"

/*
 * Debug Module tailored for the es_es adv3 library, version 1.0
 * Copyright (C) 2012, Sergi Reyner
 * Comments and bug reports should be sent to:
 * sergi.reyner@gmail.com
 *
 */

/* So, how should you assign vocabulary words to your rooms?  They
 * shouldn't be available in the release version of your game, only in
 * the debug version.  Tads 3 defines the preprocessor symbol "__DEBUG"
 * when it compiles a game with debug information.  You could simply
 * define a macro like this:
 *
 *   #ifdef __DEBUG
 *     #define dbgNoun(x) vocabWords_ = x
 *   #else
 *     #define dbgNoun(x)
 *   #endif
 *
 * And assign nouns to your rooms like this:
 *
 *   kitchen: Room {
 *     'Kitchen'  'the kitchen'
 *     "You are not in front of a white house, but in the kitchen of a
 *     yellow one. "
 *     dbgNoun('kitchen-room');
 *   }
 */

#ifdef __DEBUG
#define dbgNoun(x) vocabWords_ = x
#else
#define dbgNoun(x)
#endif


/*   With this macro in place, the definition can be shortened to:
 *
 *      dbgCommand('chapter5',
 *      {
 *          // carry out the necessary steps here
 *      });
 */
 
#ifdef __DEBUG
#define dbgCommand(name, code) grammar debugCommand : name : BasicProd execute() code
#else
#define dbgCommand(name, code)
#endif

// condicional?
#ifdef __DEBUG
#define dbgTest(name, list...) Test name [list]
Test template 'testName' [testList];
#else
#define dbgTest(name, list...)
#endif
