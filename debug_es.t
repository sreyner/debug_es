#charset "utf-8"

// This code is WORK IN PROGRESS and IT EATS BABIES.
// Use at your own risk.

/*
 * Debug Module tailored for the es_es adv3 library, version 1.0
 * Copyright (C) 2012, Sergi Reyner
 * Comments and bug reports should be sent to:
 * sergi.reyner@gmail.com
 *
 * Based on code from kfDebug.t and ncDebugActions.t
 *
 * The copyright holder hereby grants the rights of usage, distribution
 * and modification of this software to everyone and for any purpose, as
 * long as this license and the copyright notice above are preserved and
 * not modified.  There is no warranty for this software.  By using,
 * distributing or modifying this software, you indicate that you accept
 * the terms and conditions of this license.
 *
 * kfDebug's licence:
 * ------------------
 * kfDebug Module, version 1
 * Copyright (C) 2010, Krister Fundin
 * Comments and bug reports should be sent to:
 * fundin@yahoo.com
 *
 * The copyright holder hereby grants the rights of usage, distribution
 * and modification of this software to everyone and for any purpose, as
 * long as this license and the copyright notice above are preserved and
 * not modified.  There is no warranty for this software.  By using,
 * distributing or modifying this software, you indicate that you accept
 * the terms and conditions of this license.
 *
 * ncDebugActions' licence:
 * ------------------------
 * Real NC Debug Actions Module, version 1.1
 * Copyright (C) 2002, Nikos Chantziaras.
 * Comments and bug reports should be sent to:
 * realnc@lycos.de
 *
 * The copyright holder hereby grants the rights of usage, distribution
 * and modification of this software to everyone and for any purpose, as
 * long as this license and the copyright notice above are preserved and
 * not modified.  There is no warranty for this software.  By using,
 * distributing or modifying this software, you indicate that you accept
 * the terms and conditions of this license.
 *
 *
 * Changes:
 * --------
 *
 * 1.0:
 * ----
 *
 * Translated to Spanish
 * Renamed nc's commands/verbs to something more in line with inform 7 commands/verbs
 * Renamed custom debug command's verb to dbg so as to not interfere with tads' debug verb
 * Fixed the purloin command to reveal hidden objects after taking them
 *
 *
 *
 * Instructions:
 * -------------
 *
 * - Add debug_es.t to the list of source files
 * 
 * - Optionally include debug_es.h in any source file where you want
 * to use the provided macros
 *
 * - Compile with debug enabled (t3make -d)
 *
 * 
 */

#ifdef __DEBUG

#include <adv3.h>
#include <es_es.h>
#include "debug_es.h"

// yes, yes, this shouldn't be done with an include, I know...
#include <reflect.t>

/* --------------------------------------------------------------------
 * Purloin
 *
 * This verb lets you pick up any takeable item, even if it's not
 * accessible to you - say it's in a different room or inside a closed
 * container or whatever.
 *
 * I gave it the silly name "purloin" as "purloin" is not a verb I'm likely
 * to use in the actual game for anything.  You can, of course, change
 * this to something more serious.
 *
 * Essentially, all this verb does is call the dobjFor(Take) handler.
 * The reason I have it do that instead of, say, moving the object
 * directly into the player's inventory, is because this way I can rely
 * on the standard Take checks to ensure that the player isn't trying
 * to pick up a fixed or decoration object or whatever.  Also, the
 * player can't pick up more items than he or she normally can (unless
 * the "shazam" verb has been used; see below.)
 *
 * However, "purloin" differs from the normal take verb in that instead
 * of checking to see if the object in question is in scope or not, it
 * simply lets you take any object that's anywhere in the game.  It
 * does this by overriding the objInScope() method of TAction.  Purloin
 * also doesn't care if the object is in a closed container.  This is
 * done by modifying the checkMoveViaPath() method of Container.
 * Furthermore, the only preconditions for purloin are objNotWorn (the
 * object must not currently being worn) and roomToHoldObj (there must
 * be enough room for the object in the PC's inventory).
 */

DefineTAction(Purloin)
    // We want all objects to be in scope, so we always return true
    // here.
    objInScope( obj ) { return true; }

    execAction()
    {
        if (!gActor.isPlayerChar) {
            libMessages.systemActionToNPC();
            exit;
        }
        inherited;
	
	// reveal hidden objects
	// apparently it's ok to call discover() even on a regular Thing (?)
	gDobj.discover();
    }
;

VerbRule(Purloin)
    'purloin' dobjList
    : PurloinAction
    verbPhrase = 'obtener/obteniendo (qué)'
;

modify Thing {
    dobjFor(Purloin)
    {
        // We do not require the object to be touchable to purloin it.
        // But we still require that it's not being worn, and that the
        // PC has enough room in his inventory to hold it.
        preCond = [objNotWorn, roomToHoldObj];

        // Everything else works the same as with the Take action.
        verify() { verifyDobjTake(); }
        remap() { return remapDobjTake(); }
        check() { checkDobjTake(); }
        action() { actionDobjTake(); }
    }
}

modify Container {
    // checkMoveViaPath() is defined in the Thing class, but the
    // Container class overrides this method to check if the container
    // is closed and, if it is, disallows the action.  Since purloin must
    // be able to take objects even from closed containers, we need to
    // modify this method.
    checkMoveViaPath( obj, dest, op )
    {
        // Allow the operation if the current action is purloin.
        if (gActionIs(Purloin)) return checkStatusSuccess;

        // Otherwise, do whatever was the default.
        return inherited(obj, dest, op);
    }
}


/* --------------------------------------------------------------------
 * ShowMe
 *
 * Another useful verb with a silly name.  ShowMe lets you find an
 * object anywhere in the game without touching it.  ShowMeing an object
 * simply prints that object's current location's `name' property.
 */

DefineTAction(ShowMe)
    objInScope( obj ) { return true; }

    execAction()
    {
        if (!gActor.isPlayerChar) {
            libMessages.systemActionToNPC();
            exit;
        }
        inherited;
    }
;

VerbRule(ShowMe)
    ('showme' | 'v') dobjList
    : ShowMeAction
    verbPhrase = 'mostrar/mostrando (qué)'
;

modify Thing {
    dobjFor(ShowMe)
    {
        preCond = [];

        verify() {}
        check() {}

        action()
        {
            "\^<<self.theName>> <<self.verbToBe>> ";
            if (!self.location) {
                "flotando en el nil-espacio. ";
            } else {
                "en la localidad llamada <q><<self.location.name>></q>. ";
            }
        }
    }
}

/* We want Decoration, Intangible and Distant items to work with showme,
 * so we also define dobjFor(ShowMe) handlers for them.
 */
modify Decoration {
    dobjFor(ShowMe)
    {
        // Just do what we defined in Thing.
        preCond() { return inherited; }
        verify() { inherited; }
        check() { inherited; }
        action() { inherited; }
    }
}

modify Intangible {
    dobjFor(ShowMe)
    {
        preCond() { return inherited; }
        verify() { inherited; }
        check() { inherited; }
        action() { inherited; }
    }
}

modify Distant {
    dobjFor(ShowMe)
    {
        preCond() { return inherited; }
        verify() { inherited; }
        check() { inherited; }
        action() { inherited; }
    }
}


/* --------------------------------------------------------------------
 * GoNear
 *
 * This is a particularly useful verb when testing that zaps you
 * directly to a specific location as though you'd teleported there.
 * Saves a lot of walking around.  To use it, simply type "gonear"
 * followed by the location to which you wish to gonear, or by an object
 * that is contained either directly or indirectly in this location, so
 * that typing "gonear item" for non-room objects will take you to that
 * object's location.  This means that you can "gonear" to any room with
 * at least one object in it, even if you haven't taken the step of
 * assigning nouns to your rooms.  If the specified object is nested
 * inside multiple containers, we simply traverse the locations
 * upwards, until we find one that can contain actors.  Only
 * BasicLocation objects can do that, so we simply gonear the PC to the
 * first BasicLocation we found.
 *
 * We require that the player isn't seated (by specifying the
 * actorStanding precondition) to avoid problems.  This means that the
 * PC will attempt to stand up before we gonear him to the new location.
 * Then we transport them directly to the chosen location by using the
 * actor's moveInto() method.  In homage to the classic "Adventure", we
 * surround the player with a nice orange cloud of smoke.
 *
 * There's one more time-consuming thing that you, as implementor, have
 * to do to be able to use this verb directly with rooms.  And that is
 * you have to assign appropriate nouns and adjectives to each room you
 * want to be gonearable.  If you don't, then the only way to gonear to a
 * location is by using an item in that location as the direct object
 * of the verb.  This can be a bit awkward, since interactive
 * disambiguation is sometimes impossible with some objects, most
 * probably with decoration and fixed objects ("Which door do you mean,
 * the door, the door, the door, or the door?").
 *
 * So, how should you assign vocabulary words to your rooms?  They
 * shouldn't be available in the release version of your game, only in
 * the debug version.  Tads 3 defines the preprocessor symbol "__DEBUG"
 * when it compiles a game with debug information.  You could simply
 * define a macro like this:
 *
 *   #ifdef __DEBUG
 *     #define dbgNoun(x) vocabWords_ = x
 *   #else
 *     #define dbgNoun(x)
 *   #endif
 *
 * And assign nouns to your rooms like this:
 *
 *   kitchen: Room {
 *     'Kitchen'  'the kitchen'
 *     "You are not in front of a white house, but in the kitchen of a
 *     yellow one. "
 *     dbgNoun('kitchen-room');
 *   }
 *
 * Now you can gonear to the kitchen like this:
 *
 *   >GONEAR KITCHEN-ROOM
 */

DefineTAction(GoNear)
    objInScope( obj ) { return true; }

    execAction()
    {
        if (!gActor.isPlayerChar) {
            libMessages.systemActionToNPC();
            exit;
        }
        inherited;
    }
;

VerbRule(GoNear)
    'gonear' singleDobj
    : GoNearAction
    verbPhrase = 'ir/yendo (a dónde)'
;

modify Thing {
    dobjFor(GoNear)
    {
        // We must be standing to be able to gonear.
        preCond = [actorStanding];

        verify() {}

        check() {}

        action()
        {
            // Destination.
            local dest = gDobj.ofKind(BasicLocation) ? gDobj : gDobj.location;

            // We do a "location rewind" until we find a location that
            // can contain actors.
            while (dest) {
                if (dest == gPlayerChar.location) {
                    // The location we found is the same as the PC's
                    // current location.  No need to gonear.
                    "Ya estás ahí. ";
                    return;
                }
                if (dest.ofKind(BasicLocation)) {
                    // We found a BasicLocation.  Let's gonear there.
                    "Cruzas el espacio interdimensional hasta llegar a tu destino. ";

                    // Remember my original location - this is the
                    // location where the PC was before he goneared.
                    local origin = gPlayerChar.location;

                    // Tell the old room we're leaving.
                    if (origin) origin.travelerLeaving(gPlayerChar, dest, nil);

                    // Move to the destination.
                    gPlayerChar.moveIntoForTravel(dest);

                    // Tell the new room we're arriving, if we changed
                    // locations.
                    if (gPlayerChar.location)
                        gPlayerChar.location.travelerArriving(gPlayerChar, origin, nil, nil);

                    // We're done.
                    return;
                }
                // One level up.
                dest = dest.location;
            }
            // We didn't find a BasicLocation.  We can't gonear.
            "No puedo transportarte cerca de ahí. ";
        }
    }
}

/* We want Decoration, Intangible and Distant items to work with gonear,
 * so we also define dobjFor(GoNear) handlers for them.
 */
modify Decoration {
    dobjFor(GoNear)
    {
        preCond() { return inherited; }
        verify() { inherited; }
        check() { inherited; }
        action() { inherited; }
    }
}

modify Intangible {
    dobjFor(GoNear)
    {
        preCond() { return inherited; }
        verify() { inherited; }
        check() { inherited; }
        action() { inherited; }
    }
}

modify Distant {
    dobjFor(GoNear)
    {
        preCond() { return inherited; }
        verify() { inherited; }
        check() { inherited; }
        action() { inherited; }
    }
}


/* --------------------------------------------------------------------
 * Shazam
 *
 * Sometimes, in a game, you need to pick up and carry a whole plethora
 * of items, as the standard definition for the player character object
 * has set limits as to the number of objects (bulkCapacity), the
 * maximum bulk of any one object (maxSingleBulk) and the total weight
 * of objects (weightCapacity) the player can carry.  This is obviously
 * important from both a realism standpoint and also a puzzle-making
 * one.  However, it can be a total pain in the neck when testing.
 *
 * One of the features of the shazam verb is that it increases the
 * strength and carrying ability of the player to superhuman levels.
 * Use the unshazam command to restore the player to his or her normal
 * abilities.
 *
 * The other feature of the verb involves dark rooms.  Sometimes, when
 * playing, it's a hassle to have to carry a torch or other light
 * source all the time.  However, through the miracle of the shazam verb,
 * the player can set him or herself literally glowing, thereby
 * obviating the light source problem.  People who've played any of the
 * Infocom fantasies will recognize this as a kind of "frotz me"
 * command.  Again, unshazam makes the player normal once more.
 */

class ShazamAction: IAction {
    // Is the PC currently in Shazam mode? (Class property)
    playerIsShazam = nil;

    // We save the old values so that we can restore them later with
    // the Unshazam action. (Class properties)
    oldBulkCapacity   = gPlayerChar.bulkCapacity;
    oldMaxSingleBulk  = gPlayerChar.maxSingleBulk;
    oldWeightCapacity = gPlayerChar.weightCapacity;
    oldBrightness     = gPlayerChar.brightness;

    execAction()
    {
        if (!gActor.isPlayerChar) {
            libMessages.systemActionToNPC();
            exit;
        }

        if (ShazamAction.playerIsShazam) {
            gPlayerChar.bulkCapacity   = ShazamAction.oldBulkCapacity;
            gPlayerChar.maxSingleBulk  = ShazamAction.oldMaxSingleBulk;
            gPlayerChar.weightCapacity = ShazamAction.oldWeightCapacity;
            gPlayerChar.brightness     = ShazamAction.oldBrightness;

            ShazamAction.playerIsShazam = nil;

            "Pierdes tu fuerza sobrehumana y dejar de brillar. ";
        }
        else {
            ShazamAction.oldBulkCapacity   = gPlayerChar.bulkCapacity;
            ShazamAction.oldMaxSingleBulk  = gPlayerChar.maxSingleBulk;
            ShazamAction.oldWeightCapacity = gPlayerChar.weightCapacity;
            ShazamAction.oldBrightness     = gPlayerChar.brightness;
            
            gPlayerChar.bulkCapacity   = 65535;
            gPlayerChar.maxSingleBulk  = 65535;
            gPlayerChar.weightCapacity = 65535;
            gPlayerChar.brightness     = 4;

            ShazamAction.playerIsShazam = true;

            "Adquieres fuerza sobrehumana y empiezas a brillar. ";
        }
    }
}

VerbRule(Shazam)
    'shazam'
    : ShazamAction
    verbPhrase = 'shazam/switching superhuman powers'
;


/* ---------------------------------------------------------------------- */
/*
 *   Game-specific debugging commands. These can be used E.G. to jump to a
 *   later chapter in the game. To add one, use this syntax:
 *
 *      grammar debugCommand:
 *          'chapter5'
 *          : BasicProd
 *
 *          execute()
 *          {
 *              // carry out the necessary steps here
 *          }
 *      ;
 *
 *   A definition such as this won't have any effect in a release build, so
 *   it's not necessary to put an #ifdef around it. One might want to set up
 *   a macro for creating these debug commands in order to save some typing,
 *   however. Here is one way to do so:
 *
 *      #ifdef __DEBUG
 *      #define dbgCommand(name, code) \
 *          grammar debugCommand : name : BasicProd execute() code
 *      #else
 *      #define dbgCommand(name, code)
 *      #endif
 *
 *   With this macro in place, the definition can be shortened to:
 *
 *      dbgCommand('chapter5',
 *      {
 *          // carry out the necessary steps here
 *      });
 *
 *   In any case, the command is then executed by typing 'dbg chapter5'.
 */
DefineSystemAction(CustomDebug)
    execSystemAction()
    {
        cmd_.execute();
    }
;

VerbRule(CustomDebug)
    'dbg' debugCommand->cmd_
    : CustomDebugAction
    
    verbPhrase = 'debug/debugging'
;

grammar debugCommand(unknown):
    [badness 500] miscWordList
    : BasicProd
    
    execute()
    {
        "Comando de depuración desconocido. ";
    }
;



/*
To use this extension, which was created by Ben Cressey and Eric Eve, define Test objects like so:
    
foo: Test
    testName = 'foo'
    testList =
    [
        'x me',
        'i'
    ]
;

bar: Test
    testName = 'bar'
    testList =
    [
        'look',
        'listen'
    ]
;

all: Test
    testName = 'all'
    testList =
    [
        'test foo',
        'test bar'
    ]
; 

Alternatively, you can include the following line at the head of your file of test scripts:
    
Test template 'testName' [testList];

...and then use the template structure to create your test objects more conveniently:
    
someTest: Test 'foo' ['x me', 'i'];

Unless you're planning to refer to the Test object in some other part of your code,
you can save a bit of typing by making it an anonymous object:
    
Test 'foo' ['x me', 'i'];  

*/
    
/*
 *   The 'list tests' and 'list tests fully' commands can be used to list 
 *   your test scripts from within the running game.
 */

DefineSystemAction(ListTests)
    execSystemAction
    {

        if(allTests.lst.length == 0)
        {
            reportFailure('No hay secuencias de test definidas en este juego. ');
            exit;
        }
       
        foreach(local testObj in allTests.lst)
        {
            "<<testObj.testName>>";
            if(gAction.fully)               
            {
                ": ";
                foreach(local txt in testObj.testList)
                    "<<txt>>/";
            }
            "\n";
        }
    }
    fully = nil
;

VerbRule(ListTests)
    ('list' | 'l') 'tests' (| 'fully' -> fully)
    : ListTestsAction
    verbPhrase = 'listar/listando las secuencias de test'
;

    /*
     *   The 'test X' command can be used with any Test object defined in the source code:
     */

DefineLiteralAction(Test)
   execAction()
   {
      local target = getLiteral().toLower();
      local script = allTests.valWhich({x: x.testName.toLower == target});
      if (script) {
	  script.run();
      }
      else
         "Secuencia de test no encontrada. ";
   }
;

VerbRule(Test)
   'test' singleLiteral
   : TestAction
   verbPhrase = 'testear/testeando (qué)'
;

class Test: object
   testName = 'nil'
   testList = [ 'z' ]

   run
   {
      "Testeando secuencia: \"<<testName>>\". ";
      foreach(local str in self.testList) {
	  local toks = Tokenizer.tokenize(str);
	  executeCommand(gPlayerChar, gPlayerChar, toks, true);
      }
   }
;

allTests: object
   lst()
   {
      if (lst_ == nil)
         initLst();
      return lst_;
   }

   initLst()
   {
      lst_ = new Vector(50);
      local obj = firstObj();
      while (obj != nil)
      {
         if(obj.ofKind(Test))
            lst_.append(obj);
         obj = nextObj(obj);
      }
      lst_ = lst_.toList();
   }

   valWhich(cond)
   {
      if (lst_ == nil)
         initLst();
      return lst_.valWhich(cond);
   }

   lst_ = nil
;


#endif


// desactivada porque no parece ser muy útil
#ifdef SEGUROQUEESTONOESTADEFINIDO

/* ---------------------------------------------------------------------- */
/*
 *   In-game expression evaluation. This part of the extension provides a
 *   quick way to look up variables from within the game. Any command line
 *   which starts with an equals sign is passed through a much simplified
 *   version of the 'evaluate' command from the TADS 3 debugger, and the
 *   result is then displayed if there was one.
 *
 *   The parser used here can evaluate properties and call methods on
 *   objects, possibly with arguments, and it recognizes simple strings and
 *   numbers, but that's as far as it goes. In particular, it should be
 *   noted that macros such as gDobj can't be evaluated this way.
 */

evalPreParser: StringPreParser
    doParsing(str, which)
    {
        if (which == rmcAskLiteral)
            return str;
        
        if (!str.startsWith('='))
            return str;
        
        try
        {
            local toks = evalTokenizer.tokenize(str.substr(2));
            local match = evalExpr.parseTokens(toks, nil);
            
            if (match != [])
            {
                say (reflectionServices.valToSymbol(match[1].evaluate()));
                return nil;
            }
        }
        catch (Exception exc)
        {
        }
        
        return str;
    }
;

evalException: Exception;

evalTokenizer: Tokenizer
    rules_ = static
    [
        ['whitespace', new RexPattern('<space>+'), nil, &tokCvtSkip, nil],
        ['operators', new RexPattern('[.,()]'), tokPunct, nil, nil],
        ['strings', new RexPattern('<squote>.*?<squote>'),
            tokString, nil, nil],
        ['numbers', new RexPattern('<digit>+'), tokInt, nil, nil],
        ['symbols', new RexPattern('<alphanum|_>+'), tokWord, nil, nil]
    ]
;

grammar evalExpr(compound):
    evalExpr->lhs_ '.' evalObj->rhs_ evalArgList->args_
    : BasicProd
    
    evaluate()
    {
        local lhs = lhs_.evaluate();
        local rhs = rhs_.evaluate();
        local args = args_.evaluate();
        
        if (dataType(lhs) not in (TypeObject, TypeSString, TypeList)
            || dataType(rhs) != TypeProp)
        {
            throw evalException;
        }
        
        return lhs.(rhs)(args...);
    }
;

grammar evalExpr(simple):
    evalObj->obj_
    : BasicProd
    
    evaluate() { return obj_.evaluate(); }
;

grammar evalArgList(nonempty):
    '(' evalArgs->args_ ')'
    : BasicProd
    
    evaluate() { return args_.evaluate(); }
;

grammar evalArgList(empty):
    ('(' ')' | )
    : BasicProd
    
    evaluate() { return []; }
;

grammar evalArgs(compound):
    evalExpr->arg_ ',' evalArgs->args_
    : BasicProd
    
    evaluate() { return args_.evaluate().prepend(arg_.evaluate()); }
;

grammar evalArgs(single):
    evalExpr->arg_
    : BasicProd
    
    evaluate() { return [arg_.evaluate()]; }
;

grammar evalObj(symbol):
    tokWord->tok_
    : BasicProd
    
    evaluate()
    {
        switch (tok_)
        {
        case 'true': return true;
        case 'nil': return nil;
        default:
            local val = reflectionServices.symtab_[tok_];
        
            if (val != nil)
                return val;
            else
                throw evalException;
        }
    }
;

grammar evalObj(string):
    tokString->tok_
    : BasicProd
    
    evaluate() { return tok_.substr(2, tok_.length - 2); }
;

grammar evalObj(number):
    tokInt->tok_
    : BasicProd
    
    evaluate() { return toInteger(tok_); }
;
#endif

